
//////////////////////////////////////
//
// useRef
//
//////////////////////////////////////

function updateWorkInProgressHook(): Hook {
  // This function is used both for updates and for re-renders triggered by a
  // render phase update. It assumes there is either a current hook we can
  // clone, or a work-in-progress hook from a previous render pass that we can
  // use as a base. When we reach the end of the base list, we must switch to
  // the dispatcher used for mounts.
  if (nextWorkInProgressHook !== null) {
    // There's already a work-in-progress. Reuse it.
    workInProgressHook = nextWorkInProgressHook;
    nextWorkInProgressHook = workInProgressHook.next;

    currentHook = nextCurrentHook;
    nextCurrentHook = currentHook !== null ? currentHook.next : null;
  } else {
    // Clone from the current hook.
    invariant(
      nextCurrentHook !== null,
      'Rendered more hooks than during the previous render.',
    );
    currentHook = nextCurrentHook;

    const newHook: Hook = {
      memoizedState: currentHook.memoizedState,

      baseState: currentHook.baseState,
      queue: currentHook.queue,
      baseUpdate: currentHook.baseUpdate,

      next: null,
    };

    if (workInProgressHook === null) {
      // This is the first hook in the list.
      workInProgressHook = firstWorkInProgressHook = newHook;
    } else {
      // Append to the end of the list.
      workInProgressHook = workInProgressHook.next = newHook;
    }
    nextCurrentHook = currentHook.next;
  }
  return workInProgressHook;
}

function mountRef<T>(initialValue: T): { current: T } {
  const hook = mountWorkInProgressHook();
  const ref = { current: initialValue };
  if (__DEV__) {
    Object.seal(ref);
  }
  hook.memoizedState = ref;
  return ref;
}

function updateRef<T>(initialValue: T): { current: T } {
  const hook = updateWorkInProgressHook();
  return hook.memoizedState;
}

export const ContextOnlyDispatcher: Dispatcher = {
  useRef: throwInvalidHookError,
};

const HooksDispatcherOnMount: Dispatcher = {
  useRef: mountRef,
};

const HooksDispatcherOnUpdate: Dispatcher = {
  useRef: updateRef,
};


const HooksDispatcherOnMountInDEV = {

  useRef<T>(initialValue: T): { current: T } {
    currentHookNameInDev = 'useRef';
    mountHookTypesDev();
    return mountRef(initialValue);
  },
};

const HooksDispatcherOnMountWithHookTypesInDEV = {

  useRef<T>(initialValue: T): { current: T } {
    currentHookNameInDev = 'useRef';
    updateHookTypesDev();
    return mountRef(initialValue);
  },
};

HooksDispatcherOnUpdateInDEV = {

  useRef<T>(initialValue: T): { current: T } {
    currentHookNameInDev = 'useRef';
    updateHookTypesDev();
    return updateRef(initialValue);
  },
};

InvalidNestedHooksDispatcherOnMountInDEV = {

  useRef<T>(initialValue: T): { current: T } {
    currentHookNameInDev = 'useRef';
    warnInvalidHookAccess();
    mountHookTypesDev();
    return mountRef(initialValue);
  },
};

InvalidNestedHooksDispatcherOnUpdateInDEV = {

  useRef<T>(initialValue: T): { current: T } {
    currentHookNameInDev = 'useRef';
    warnInvalidHookAccess();
    updateHookTypesDev();
    return updateRef(initialValue);
  },
};
