//////////////////////////////////////
//
// useResponder
//
//////////////////////////////////////

import { createResponderListener } from './ReactFiberEvents';

export const ContextOnlyDispatcher: Dispatcher = {
  useResponder: throwInvalidHookError,
};

const HooksDispatcherOnMount: Dispatcher = {
  useResponder: createResponderListener,
};

const HooksDispatcherOnUpdate: Dispatcher = {
  useResponder: createResponderListener,
};


const HooksDispatcherOnMountInDEV = {
  useResponder<E, C>(
    responder: ReactEventResponder<E, C>,
    props,
  ): ReactEventResponderListener<E, C> {
    currentHookNameInDev = 'useResponder';
    mountHookTypesDev();
    return createResponderListener(responder, props);
  },
};

const HooksDispatcherOnMountWithHookTypesInDEV = {
  useResponder<E, C>(
    responder: ReactEventResponder<E, C>,
    props,
  ): ReactEventResponderListener<E, C> {
    currentHookNameInDev = 'useResponder';
    updateHookTypesDev();
    return createResponderListener(responder, props);
  },
};

const HooksDispatcherOnUpdateInDEV = {
  useResponder<E, C>(
    responder: ReactEventResponder<E, C>,
    props,
  ): ReactEventResponderListener<E, C> {
    currentHookNameInDev = 'useResponder';
    updateHookTypesDev();
    return createResponderListener(responder, props);
  },
};

const InvalidNestedHooksDispatcherOnMountInDEV = {
  useResponder<E, C>(
    responder: ReactEventResponder<E, C>,
    props,
  ): ReactEventResponderListener<E, C> {
    currentHookNameInDev = 'useResponder';
    warnInvalidHookAccess();
    mountHookTypesDev();
    return createResponderListener(responder, props);
  },
};

const InvalidNestedHooksDispatcherOnUpdateInDEV = {
  useResponder<E, C>(
    responder: ReactEventResponder<E, C>,
    props,
  ): ReactEventResponderListener<E, C> {
    currentHookNameInDev = 'useResponder';
    warnInvalidHookAccess();
    updateHookTypesDev();
    return createResponderListener(responder, props);
  },
};
