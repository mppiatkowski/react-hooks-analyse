
//////////////////////////////////////
//
// useContext
//
//////////////////////////////////////

import { readContext } from './ReactFiberNewContext';

export const ContextOnlyDispatcher: Dispatcher = {
  readContext,

  useContext: throwInvalidHookError,
};

const HooksDispatcherOnMount: Dispatcher = {
  readContext,

  useContext: readContext,
};

const HooksDispatcherOnUpdate: Dispatcher = {
  readContext,

  useContext: readContext,
};


const HooksDispatcherOnMountInDEV = {
  readContext<T>(
    context: ReactContext<T>,
    observedBits: void | number | boolean,
  ): T {
    return readContext(context, observedBits);
  },
};

const HooksDispatcherOnMountWithHookTypesInDEV = {
  readContext<T>(
    context: ReactContext<T>,
    observedBits: void | number | boolean,
  ): T {
    return readContext(context, observedBits);
  },
};

const HooksDispatcherOnUpdateInDEV = {
  readContext<T>(
    context: ReactContext<T>,
    observedBits: void | number | boolean,
  ): T {
    return readContext(context, observedBits);
  },
};

InvalidNestedHooksDispatcherOnMountInDEV = {
  readContext<T>(
    context: ReactContext<T>,
    observedBits: void | number | boolean,
  ): T {
    warnInvalidContextAccess();
    return readContext(context, observedBits);
  },
};

InvalidNestedHooksDispatcherOnUpdateInDEV = {
  readContext<T>(
    context: ReactContext<T>,
    observedBits: void | number | boolean,
  ): T {
    warnInvalidContextAccess();
    return readContext(context, observedBits);
  },
};
