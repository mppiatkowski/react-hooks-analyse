//////////////////////////////////////
//
// useLayoutEffect
//
//////////////////////////////////////


function throwInvalidHookError() {
  invariant(
    false,
    'Invalid hook call. Hooks can only be called inside of the body of a function component. This could happen for' +
    ' one of the following reasons:\n' +
    '1. You might have mismatching versions of React and the renderer (such as React DOM)\n' +
    '2. You might be breaking the Rules of Hooks\n' +
    '3. You might have more than one copy of React in the same app\n' +
    'See https://fb.me/react-invalid-hook-call for tips about how to debug and fix this problem.',
  );
}

function mountLayoutEffect(
  create: () => (() => void) | void,
  deps: Array<mixed> | void | null,
): void {
  return mountEffectImpl(
    UpdateEffect,
    UnmountMutation | MountLayout,
    create,
    deps,
  );
}

function updateLayoutEffect(
  create: () => (() => void) | void,
  deps: Array<mixed> | void | null,
): void {
  return updateEffectImpl(
    UpdateEffect,
    UnmountMutation | MountLayout,
    create,
    deps,
  );
}

const ContextOnlyDispatcher: Dispatcher = {
  useLayoutEffect: throwInvalidHookError,
};

const HooksDispatcherOnMount: Dispatcher = {
  useLayoutEffect: mountLayoutEffect,
};

const HooksDispatcherOnUpdate: Dispatcher = {
  useLayoutEffect: updateLayoutEffect,
};

const HooksDispatcherOnMountInDEV = {

  useLayoutEffect(
    create: () => (() => void) | void,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useLayoutEffect';
    mountHookTypesDev();
    checkDepsAreArrayDev(deps);
    return mountLayoutEffect(create, deps);
  },
};

HooksDispatcherOnMountWithHookTypesInDEV = {

  useLayoutEffect(
    create: () => (() => void) | void,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useLayoutEffect';
    updateHookTypesDev();
    return mountLayoutEffect(create, deps);
  },
};

HooksDispatcherOnUpdateInDEV = {

  useLayoutEffect(
    create: () => (() => void) | void,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useLayoutEffect';
    updateHookTypesDev();
    return updateLayoutEffect(create, deps);
  },
};

InvalidNestedHooksDispatcherOnMountInDEV = {

  useLayoutEffect(
    create: () => (() => void) | void,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useLayoutEffect';
    warnInvalidHookAccess();
    mountHookTypesDev();
    return mountLayoutEffect(create, deps);
  },
};

InvalidNestedHooksDispatcherOnUpdateInDEV = {

  useLayoutEffect(
    create: () => (() => void) | void,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useLayoutEffect';
    warnInvalidHookAccess();
    updateHookTypesDev();
    return updateLayoutEffect(create, deps);
  },
};
