
//////////////////////////////////////
//
// useDebugValue
//
//////////////////////////////////////


function throwInvalidHookError() {
  invariant(
    false,
    'Invalid hook call. Hooks can only be called inside of the body of a function component. This could happen for' +
    ' one of the following reasons:\n' +
    '1. You might have mismatching versions of React and the renderer (such as React DOM)\n' +
    '2. You might be breaking the Rules of Hooks\n' +
    '3. You might have more than one copy of React in the same app\n' +
    'See https://fb.me/react-invalid-hook-call for tips about how to debug and fix this problem.',
  );
}

function mountDebugValue<T>(value: T, formatterFn: ?(value: T) => mixed): void {
  // This hook is normally a no-op.
  // The react-debug-hooks package injects its own implementation
  // so that e.g. DevTools can display custom hook values.
}

const updateDebugValue = mountDebugValue;

export const ContextOnlyDispatcher: Dispatcher = {
  useDebugValue: throwInvalidHookError,
};

const HooksDispatcherOnMount: Dispatcher = {
  useDebugValue: mountDebugValue,
};

const HooksDispatcherOnUpdate: Dispatcher = {
  useDebugValue: updateDebugValue,
};

const HooksDispatcherOnMountInDEV = {
  useDebugValue<T>(value: T, formatterFn: ?(value: T) => mixed): void {
    currentHookNameInDev = 'useDebugValue';
    mountHookTypesDev();
    return mountDebugValue(value, formatterFn);
  },
};

const HooksDispatcherOnMountWithHookTypesInDEV = {
  useDebugValue<T>(value: T, formatterFn: ?(value: T) => mixed): void {
    currentHookNameInDev = 'useDebugValue';
    updateHookTypesDev();
    return mountDebugValue(value, formatterFn);
  },
};

const HooksDispatcherOnUpdateInDEV = {

  useDebugValue<T>(value: T, formatterFn: ?(value: T) => mixed): void {
    currentHookNameInDev = 'useDebugValue';
    updateHookTypesDev();
    return updateDebugValue(value, formatterFn);
  },
};

const InvalidNestedHooksDispatcherOnMountInDEV = {
  useDebugValue<T>(value: T, formatterFn: ?(value: T) => mixed): void {
    currentHookNameInDev = 'useDebugValue';
    warnInvalidHookAccess();
    mountHookTypesDev();
    return mountDebugValue(value, formatterFn);
  },
};

const InvalidNestedHooksDispatcherOnUpdateInDEV = {
  useDebugValue<T>(value: T, formatterFn: ?(value: T) => mixed): void {
    currentHookNameInDev = 'useDebugValue';
    warnInvalidHookAccess();
    updateHookTypesDev();
    return updateDebugValue(value, formatterFn);
  },
}
