//////////////////////////////////////
//
// useImperativeHandle
//
//////////////////////////////////////


function imperativeHandleEffect<T>(
  create: () => T,
  ref: { current: T | null } | ((inst: T | null) => mixed) | null | void,
) {
  if (typeof ref === 'function') {
    const refCallback = ref;
    const inst = create();
    refCallback(inst);
    return () => {
      refCallback(null);
    };
  } else if (ref !== null && ref !== undefined) {
    const refObject = ref;
    if (__DEV__) {
      warning(
        refObject.hasOwnProperty('current'),
        'Expected useImperativeHandle() first argument to either be a ' +
        'ref callback or React.createRef() object. Instead received: %s.',
        'an object with keys {' + Object.keys(refObject).join(', ') + '}',
      );
    }
    const inst = create();
    refObject.current = inst;
    return () => {
      refObject.current = null;
    };
  }
}

function mountImperativeHandle<T>(
  ref: { current: T | null } | ((inst: T | null) => mixed) | null | void,
  create: () => T,
  deps: Array<mixed> | void | null,
): void {
  if (__DEV__) {
    warning(
      typeof create === 'function',
      'Expected useImperativeHandle() second argument to be a function ' +
      'that creates a handle. Instead received: %s.',
      create !== null ? typeof create : 'null',
    );
  }

  // TODO: If deps are provided, should we skip comparing the ref itself?
  const effectDeps =
    deps !== null && deps !== undefined ? deps.concat([ref]) : null;

  return mountEffectImpl(
    UpdateEffect,
    UnmountMutation | MountLayout,
    imperativeHandleEffect.bind(null, create, ref),
    effectDeps,
  );
}

function updateImperativeHandle<T>(
  ref: { current: T | null } | ((inst: T | null) => mixed) | null | void,
  create: () => T,
  deps: Array<mixed> | void | null,
): void {
  if (__DEV__) {
    warning(
      typeof create === 'function',
      'Expected useImperativeHandle() second argument to be a function ' +
      'that creates a handle. Instead received: %s.',
      create !== null ? typeof create : 'null',
    );
  }

  // TODO: If deps are provided, should we skip comparing the ref itself?
  const effectDeps =
    deps !== null && deps !== undefined ? deps.concat([ref]) : null;

  return updateEffectImpl(
    UpdateEffect,
    UnmountMutation | MountLayout,
    imperativeHandleEffect.bind(null, create, ref),
    effectDeps,
  );
}

const ContextOnlyDispatcher: Dispatcher = {
  useImperativeHandle: throwInvalidHookError,
};

const HooksDispatcherOnMount: Dispatcher = {
  useImperativeHandle: mountImperativeHandle,
};

const HooksDispatcherOnUpdate: Dispatcher = {
  useImperativeHandle: updateImperativeHandle,
};

const HooksDispatcherOnMountInDEV = {

  useImperativeHandle<T>(
    ref: { current: T | null } | ((inst: T | null) => mixed) | null | void,
    create: () => T,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useImperativeHandle';
    mountHookTypesDev();
    checkDepsAreArrayDev(deps);
    return mountImperativeHandle(ref, create, deps);
  },
};

const HooksDispatcherOnMountWithHookTypesInDEV = {

  useImperativeHandle<T>(
    ref: { current: T | null } | ((inst: T | null) => mixed) | null | void,
    create: () => T,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useImperativeHandle';
    updateHookTypesDev();
    return mountImperativeHandle(ref, create, deps);
  },
};

const HooksDispatcherOnUpdateInDEV = {

  useImperativeHandle<T>(
    ref: { current: T | null } | ((inst: T | null) => mixed) | null | void,
    create: () => T,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useImperativeHandle';
    updateHookTypesDev();
    return updateImperativeHandle(ref, create, deps);
  },
};

const InvalidNestedHooksDispatcherOnMountInDEV = {

  useImperativeHandle<T>(
    ref: { current: T | null } | ((inst: T | null) => mixed) | null | void,
    create: () => T,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useImperativeHandle';
    warnInvalidHookAccess();
    mountHookTypesDev();
    return mountImperativeHandle(ref, create, deps);
  },
};

const InvalidNestedHooksDispatcherOnUpdateInDEV = {

  useImperativeHandle<T>(
    ref: { current: T | null } | ((inst: T | null) => mixed) | null | void,
    create: () => T,
    deps: Array<mixed> | void | null,
  ): void {
    currentHookNameInDev = 'useImperativeHandle';
    warnInvalidHookAccess();
    updateHookTypesDev();
    return updateImperativeHandle(ref, create, deps);
  },
};
