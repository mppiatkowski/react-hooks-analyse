/* eslint-disable */

import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import ExternalDeps from './examples/external-deps';
import ListenerFromEffect from './examples/listener-from-effect';
import MaxUpdateLevels from './examples/max-update-levels';
import HooksReorder from './examples/hooks-reorder';

function App() {
  const [active, setActive] = React.useState('start');

  return (
    <div className="app">
      <nav class="nav">
        <ul>
          <li>
            <buttton onClick={() => setActive('start')}>Start</buttton>
          </li>
          <li>
            <buttton onClick={() => setActive('listener-from-effect')}>Listener from effect</buttton>
          </li>
          <li>
            <buttton onClick={() => setActive('hooks-reorder')}>Reorder / replace hooks</buttton>
          </li>
          <li>
            <buttton onClick={() => setActive('external-deps')}>External dependencies</buttton>
          </li>
          <li>
            <buttton onClick={() => setActive('max-update-levels')}>Max update levels (25)</buttton>
          </li>
        </ul>
      </nav>

      {active === 'listener-from-effect' && <ListenerFromEffect />}
      {active === 'hooks-reorder' && <HooksReorder />}
      {active === 'external-deps' && <ExternalDeps />}
      {active === 'max-update-levels' && <MaxUpdateLevels />}
    </div>
  );
}

export default App;
