/* eslint-disable */

import React, { useEffect, useState } from 'react';

let sizeExternal = window.innerWidth;
window.addEventListener('resize', doSth, false);
function doSth() {
  sizeExternal = window.innerWidth;
  console.log('resized to', sizeExternal);
}

function example() {
  // #4 replacing / same as reordering
  let setItem1;
  let setItem2;
  let item1;
  let item2;
  function setState1() {
    [item1, setItem1] = React.useState(1);
  }
  function setState2() {
    [item1, setItem1] = React.useState('a');
  }
  if (sizeExternal > 800) {
    setState1();
  } else {
    setState2();
  }
  function onClickReorderedUseState() {
    setItem1(n => n + 1)
  }

  return (
    <div>
      <p>Reordering of hooks works as replacing: invoked hooks build a linked list with specific order.</p>
      <p>Here we reorder it on screen size > 800px</p>
      <p onClick={onClickReorderedUseState}>click to affect state</p>
      <p>Reordered item value: {item1}</p>
    </div>
  );
}

export default example;
