/* eslint-disable */

import React, { useEffect, useState } from 'react';

function example() {
  const [value, setValue] = useState(0)

  useEffect(() => {
    setValue(n => n + 1);
  }, [value])

  return (
    <div>
      <p>Warning: Maximum update depth exceeded (in console)</p>
    </div>
  );
}

export default example;
