/* eslint-disable */

import React, { useEffect, useState } from 'react';

function example() {
  const [value, setValue] = useState(0)

  useEffect(() => {
    function doSth() {
      const size = window.innerWidth;
      console.log('size', size);
      setValue(size);
    }
    window.addEventListener('resize', doSth, false);
    return () => {
      window.removeEventListener('resize', doSth);
    }
  }, []);

  return (
    <div>
      <p>Listener is attached by effect and sets one of state values.</p>
      <p style={{ fontSize: '2em' }}>Screen width: {value}</p>
    </div>
  );
}

export default example;
