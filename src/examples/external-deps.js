/* eslint-disable */

import React, { useEffect, useState } from 'react';

// #1 - no observable!
let sizeExternal = window.innerWidth;
window.addEventListener('resize', doSth, false);
function doSth() {
  sizeExternal = window.innerWidth;
  console.log('resized to', sizeExternal);
}

function example() {
  const [valInt, incrementValInt] = useState(0)
  const [value, setValue] = useState(0)

  // #1 - no observable!
  useEffect(() => {
    console.log('SIZE', sizeExternal);
  }, [sizeExternal]);

  return (
    <div>
      <p>Listener on window resize changes value set as useEffect dependency.</p>
      <p>Its value is taken into account only when PROPS/STATE change, even if it changes on resize (see console)</p>
      <p style={{ fontSize: '2em' }}>Screen width: {sizeExternal}</p>
      <p onClick={() => incrementValInt(n => n + 1)} style={{ fontSize: '2em' }}>{valInt} - click to increment</p>
    </div >
  );
}

export default example;
